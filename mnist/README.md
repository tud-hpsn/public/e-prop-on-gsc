1. Download MNIST dataset (https://yann.lecun.com/exdb/mnist/) and put it in filesMNIST folder.
2. Run mnist_processing.py in RNN_tf2keras_mnist
3. Run eprop_LSNN.py in RNN_tf2keras_mnist until Reading Dataset ... to generate initial weights for C code. Make sure it has same network size as c code (n_in, n_ch, n_regular, n_adaptive, n_out)
4. Run C code