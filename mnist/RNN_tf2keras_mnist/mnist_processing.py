import struct
import numpy as np
import gzip
from tqdm import tqdm
import os


#===================================================================================================
def parse_mnist(image_filename, label_filename, name):
    """ Parse MNIST data files into numpy arrays and save them to disk.

    Args:
        image_filename (str): name of compressed images file in MNIST format
        label_filename (str): name of compressed labels file in MNIST format

    Returns:
        Tuple (X,y):
            X (numpy.ndarray[np.float32]): 2D numpy array containing the loaded 
                data.  The dimensionality of the data should be 
                (num_examples x input_dim) where 'input_dim' is the full 
                dimension of the data, e.g., since MNIST images are 28x28, it 
                will be 784.  Values should be of type np.float32, and the data 
                should be normalized to have a minimum value of 0.0 and a 
                maximum value of 1.0. The normalization should be applied uniformly
                across the whole dataset, _not_ individual images.

            y (numpy.ndarray[dtype=np.uint8]): 1D numpy array containing the
                labels of the examples.  Values should be of type np.uint8 and
                for MNIST will contain the values 0-9.
    """
    with gzip.open(image_filename, 'rb') as f_in:
        data = f_in.read()
    format = '>{:d}B'.format(len(data))
    data = struct.unpack(format, data)
    data = np.array(data)
    data = data[16:len(data)] # remove header
    input_dim = 28*28
    data = data.astype(np.float32)
    data = data / np.max(data)
    num_examples = int(len(data) // input_dim)
    data = data.reshape(num_examples, input_dim)
    X = data.astype(np.float32)
    savePath = '../filesMNIST/' + name + '/features.dat'
    os.makedirs(os.path.dirname(savePath), exist_ok=True)
    with open(savePath, 'w') as outfile:
        np.savetxt(outfile, X, fmt='%-8.6f')

    with gzip.open(label_filename, 'rb') as f_in:
        data = f_in.read()
    format = '>{:d}B'.format(len(data))
    data = struct.unpack(format, data)
    data = np.array(data)
    data = data[8:len(data)] # remove header
    y = data.astype(np.uint8)

    savePath = '../filesMNIST/' + name + '/target_outputs.dat'
    os.makedirs(os.path.dirname(savePath), exist_ok=True)
    with open(savePath, 'w') as outfile:
        np.savetxt(outfile, y, fmt='%d')
    
    return (X, y)
#===================================================================================================


if __name__ == "__main__":
    DatasetPath = '../../../datasets/MNIST'
    X_tr, y_tr = parse_mnist(DatasetPath + "/train-images-idx3-ubyte.gz",
                             DatasetPath + "/train-labels-idx1-ubyte.gz", name='train')
    X_te, y_te = parse_mnist(DatasetPath + "/t10k-images-idx3-ubyte.gz",
                             DatasetPath + "/t10k-labels-idx1-ubyte.gz",  name='test')
