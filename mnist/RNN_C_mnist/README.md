
Output:

***********************
n_epoch: 0
loss train: 0.9939745
Error train: 0.3075000
Accuracy train: 69.2500
loss Test: 0.8434557
Error Test: 0.2487000
Accuracy Test: 75.1300
***********************
n_epoch: 1
loss train: 0.8331674 
Error train: 0.2380167
Accuracy train: 76.1983
loss Test: 0.7686690 
Error Test: 0.2190000
Accuracy Test: 78.1000
***********************
n_epoch: 2
loss train: 0.8161297
Error train: 0.2283167
Accuracy train: 77.1683
loss Test: 0.7568294
Error Test: 0.2014000
Accuracy Test: 79.8600
***********************
n_epoch: 3
loss train: 0.7966374
Error train: 0.2185333
Accuracy train: 78.1467
loss Test: 0.7560093
Error Test: 0.2030000
Accuracy Test: 79.7000
***********************
n_epoch: 4
loss train: 0.8017142
Error train: 0.2187000
Accuracy train: 78.1300
loss Test: 0.7935634
Error Test: 0.2164000
Accuracy Test: 78.3600
***********************
n_epoch: 5
-------------------------
New learning rate: 0.003000
-------------------------
loss train: 0.6593568
Error train: 0.1765333
Accuracy train: 82.3467
loss Test: 0.6622806
Error Test: 0.1828000
Accuracy Test: 81.7200
***********************
n_epoch: 6
loss train: 0.6466759
Error train: 0.1732166
Accuracy train: 82.6783
loss Test: 0.6312539
Error Test: 0.1675000
Accuracy Test: 83.2500
***********************
n_epoch: 7
loss train: 0.6449814
Error train: 0.1735666
Accuracy train: 82.6433
loss Test: 0.6096480
Error Test: 0.1588000
Accuracy Test: 84.1200
***********************
n_epoch: 8
loss train: 0.6397023
Error train: 0.1739166
Accuracy train: 82.6083
loss Test: 0.6558059
Error Test: 0.1759000
Accuracy Test: 82.4100